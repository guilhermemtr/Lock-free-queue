#!/bin/bash
CC = g++
RM = rm -f

SRC_PATH =
INC_PATH =

CFLAGS = -fPIC -march=native -Wall $(MODE) -std=c++11 #-I$(INC_PATH)
SFLAGS = -shared
LDFLAGS = -lpthread -lm

SOURCES = Queue.cc
OBJECTS = $(SOURCES:.cc=.o)

SOLIB = liblfqueue.so

MODE = $(RELEASE)

RELEASE = -O2
DEBUG = -g 

all: $(SOURCES) $(SOLIB)
	$(CC) $(CFLAGS) $(OBJECTS) $(SFLAGS) -o $(SOLIB) $(LDFLAGS)

$(SOLIB):$(OBJECTS)

%.o: %.cc
	$(CC) $(CFLAGS) -c $< -o $@ $(LDFLAGS)

clean:
	$(RM) $(OBJECTS) $(SOLIB)
