#include <stdlib.h>

#ifndef QUEUE_H
#define QUEUE_H

#define Q_SIZE (1 << 24)

class Queue
{
 public:
  /** Default constructor */
  inline Queue(long int size = Q_SIZE) {
    queue = (void **) malloc(size * sizeof(void*));
    nCurrentEnqueue = 0;
    currentEnqueue = 0;
    currentDequeue = 0;
    this->size = size;
  }
  
  inline void enqueue(void * data) {
    long int pos = __sync_fetch_and_add(&nCurrentEnqueue,1);
    //do not allow writes until it has space to do so
    while(pos >= currentDequeue);
    queue[pos%size] = data;
    while(!__sync_bool_compare_and_swap(&currentEnqueue,pos,pos+1));
  }

  inline void enqueue_n(void ** data, long int nelems) {
    long int beg = __sync_fetch_and_add(&nCurrentEnqueue,nelems);
    long int end = beg + nelems;
    long int pos;
    while(end >= currentDequeue);
    for(pos = 0; pos < nelems; pos++) {
    	queue[(pos + beg)%size] = data[pos];
    }
    while(!__sync_bool_compare_and_swap(&currentEnqueue,beg,end));
  }
  
  inline void * try_dequeue() {
    long int pos = currentDequeue;
    if(pos >= currentEnqueue)
      return nullptr;
    void * data = queue[pos%size];
    if(__sync_bool_compare_and_swap(&currentDequeue,pos,pos+1)) {
      return data;
    }
    return nullptr;
  }

  inline long int try_dequeue_n(void ** data, long int nelems) {
    long int beg = currentDequeue;
    nelems = beg - currentEnqueue;
    if(nelems <= 0)
      return 0;
    long int end = beg + nelems;
    long int pos = 0;
    while(pos + beg < end) {
    	data[pos] = queue[(pos + beg)%size];
    	pos++;
    }
    if(__sync_bool_compare_and_swap(&currentDequeue,beg,end)) {
      return nelems;
    }
    else return 0;
  }
  
  /** Default destructor */
  virtual inline ~Queue() {
    free(queue);
  }
  
 protected:
 private:
  volatile long int nCurrentEnqueue;
  volatile long int currentEnqueue;
  volatile long int currentDequeue;
  long int size;
  void ** queue;
};

#endif // QUEUE_H
