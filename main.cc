#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include "Queue.h"

#define N_THR 8

Queue * q;
volatile int stop = 0;

pthread_t thr[N_THR];

void *
task(void * arg)
{
  while(!stop) {
    q->enqueue(q);
    void * x = q->tryDequeue();
    assert(x == NULL || x == q);
  }
  pthread_exit(0); 
}


int
main(int argc, char ** argv)
{
  q = new Queue();
  int i;
  for(i = 0; i < N_THR; i++)
    pthread_create(&(thr[i]),NULL,task,NULL);
  sleep(10);
  stop = 1;
  for(i = 0; i < N_THR; i++)
    pthread_join(thr[i],NULL);
  delete q;
  return 0;
}
